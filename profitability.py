import click
import pandas as pd

from app.src.points import ChartPoints
from config import DATA_IMPORT


@click.command()
@click.argument('filename', type=click.Path(exists=True))
@click.option('--csv', is_flag=True, help='Whether export results to csv file')
@click.option('--plot', is_flag=True, help='Whether plot results')
def calculate(filename, csv, plot):
    # read sequence
    sequence = pd.read_csv(filename, names=['data'])

    # calculate result points, based on input data
    chart_points = ChartPoints(data=sequence)
    chart_points.calculate()

    # output path
    if csv:
        output_dir = DATA_IMPORT / 'output'
        output_dir.mkdir(parents=True, exist_ok=True)

        # export results to .csv
        chart_points.to_csv(output_dir / 'result_points.csv')

    if plot:
        # plot curves
        chart_points.plot()


if __name__ == '__main__':
    calculate()
