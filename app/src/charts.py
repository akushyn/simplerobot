import plotly.graph_objects as go

from app.src.points import ChartPoints


def plot_curves(cp: ChartPoints):
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=cp.plot_points.point, y=cp.plot_points.income,
                             line=dict(color='royalblue', width=4),
                             name='income, K'))

    fig.add_trace(go.Scatter(x=cp.plot_points.point, y=cp.plot_points.win,
                             mode='lines',
                             name='win'))

    fig.add_trace(go.Scatter(x=cp.plot_points.point, y=cp.plot_points.win_percent,
                             mode='lines',
                             name='win, %'))

    fig.add_trace(go.Scatter(x=cp.plot_points.point, y=cp.plot_points.lost,
                             mode='lines',
                             name='lost'))

    fig.add_trace(go.Scatter(x=cp.plot_points.point, y=cp.plot_points.draw_down,
                             line=dict(color='red', width=4),
                             name='draw down'))

    fig_title = f'Min = {cp.get_min()}, Max = {cp.get_max()}, Counts = {cp.get_counts()}'
    fig.update_layout(
        title=fig_title,
        xaxis_title="Points",
        yaxis_title="Profitability",
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="#7f7f7f"
        )
    )

    fig.show()
