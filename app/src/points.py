import pandas as pd


class ChartPoints:
    """
    Class provides chart points for plotting, according to predefined calculation formulas
    """
    def __init__(self, data):
        self.data = data

        self.param = 50
        self.param1 = 7
        self.param2 = -7

        self.plot_points = None

    def get_range(self):
        """
        Get points range: minimum, maximum
        """
        min_range = self.data.min()[0]
        max_range = self.data.max()[0]

        return min_range, max_range

    def get_counts(self):
        """
        Get counts of data elements
        """
        return len(self.data.index)

    def get_max(self):
        """
        Get max element of data sequence
        """
        return self.get_range()[1]

    def get_min(self):
        """
        Get min element of data sequence
        """
        return self.get_range()[0]

    def get_zero_win(self, point, win, lost):
        """
        Get zero-win value
        :param point: The pips value
        :param win: The win value
        :param lost: The lost value
        """
        count = self.data[self.data >= point].count()[0]
        return count - win - lost

    @staticmethod
    def get_win(values):
        """
        Get win value
        :param values:
        """
        return sum([1 for x in values if x > 0])

    @staticmethod
    def get_lost(values):
        """
        Get lost value
        :param values:
        """
        return sum([1 for x in values if x < 0])

    def get_win_percent(self, win, lost, zero_win):
        """
        Get win value in percent representation
        :param win:
        :param lost:
        :param zero_win:
        """
        return win / (win + lost + zero_win)

    def get_draw_down(self, values):
        """
        Get draw down value
        :param values:
        """
        new_values = [values[0]]
        for i in range(1, len(values)):
            new_values.append(new_values[i-1] + values[i])

        return min(new_values)

    def get_income(self, values):
        """
        Get income value
        :param values:
        """
        return sum(values)

    def _calc_point(self, point: int, normalize: bool = True) -> pd.Series:
        """
        Calculate 'win', 'win_percent', 'lost', 'income', 'point' values for each point
        :param point: The pips value
        """
        values = []
        for i in range(1, len(self.data)):
            if self.data.iloc[i-1][0] > point:
                value = self.data.iloc[i][0] * self.param2 * self.param
            else:
                value = 0
            values.append(value)

        # win counts
        win = self.get_win(values)

        # lost counts
        lost = self.get_lost(values)

        # zero win
        zero_win = self.get_zero_win(point, win, lost)

        # win counts, %
        win_percent = self.get_win_percent(win, lost, zero_win)

        # income value
        income = self.get_income(values)

        # draw-down value
        draw_down = self.get_draw_down(values)

        if normalize:
            return pd.Series(
                data=[win, int(win_percent * 100), zero_win, lost, income / 1000, draw_down / 1000, point],
                index=['win', 'win_percent', 'zero_win', 'lost', 'income', 'draw_down', 'point'],
            )

        return pd.Series(
            data=[win, win_percent, zero_win, lost, income, draw_down, point],
            index=['win', 'win_percent', 'zero_win', 'lost', 'income', 'draw_down', 'point'],
        )

    def calculate(self) -> pd.DataFrame:
        """
        Calculate all required points to plot chart curves
        """
        # skip min_points for now
        _, max_points = self.get_range()

        plot_points = pd.DataFrame(
            columns=['win', 'win_percent', 'zero_win', 'lost', 'income', 'draw_down', 'point']
        )
        for p in range(max_points - 1):
            plot_points = plot_points.append(self._calc_point(p+1), ignore_index=True)

        self.plot_points = plot_points

        return plot_points

    def to_csv(self, path):
        if self.plot_points is None:
            # export empty Dataframe
            pd.DataFrame(
                columns=['win', 'win_percent', 'zero_win', 'lost', 'income', 'draw_down', 'point']
            ).to_csv(path, index=False)
        else:
            self.plot_points.to_csv(path, index=False)

    def plot(self):
        from app.src.charts import plot_curves
        if self.plot_points is None:
            print("No calculations")
        else:
            plot_curves(self)
