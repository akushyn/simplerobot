# Simple Robot Profitability

Text and visual representation of profitability of "Simple Robot" algorithm caclulations

## Installation

Use git command to clone the project.

```bash
git clone https://gitlab.com/akushyn/simplerobot.git
```

Go to the root of simplerobot directory.
```bash
cd simplerobot
```

Create virtual environment and activate
```bash
python3 -m venv venv
source venv/bin/activate
```

Install the project requirements into virtual environment
```bash
pip3 install -r requirements.txt
```


## Usage

```
python3 profitability.py [OPTIONS] FILENAME
```

## Examples

Display help of usage
```
python3 profitability.py --help
```

Export results to data/output/*.csv
```
python3 profitability.py --csv sample_sequence.txt
```

Plot results
```
python3 profitability.py --plot sample_sequence.txt
```

Plot results and export to data/output/*.csv
```
python3 profitability.py --csv --plot sample_sequence.txt
```

![](resources/sample_plots.png)
