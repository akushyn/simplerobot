import os
from pathlib import Path

APP_ROOT = os.path.join(os.path.dirname(__file__))
DATA_IMPORT = Path(APP_ROOT) / 'data'
